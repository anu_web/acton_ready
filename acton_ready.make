core = 7.x
api = 2

projects[drupal][type] = "core"

; Contrib modules - core extensions

projects[libraries][type] = "module"
projects[libraries][subdir] = "contrib"

projects[jquery_update][type] = "module"
projects[jquery_update][subdir] = "contrib"

projects[token][type] = "module"
projects[token][subdir] = "contrib"

projects[pathauto][type] = "module"
projects[pathauto][subdir] = "contrib"

; Contrib modules - other extensions

projects[entity][type] = "module"
projects[entity][subdir] = "contrib"

projects[rules][type] = "module"
projects[rules][subdir] = "contrib"

projects[ctools][type] = "module"
projects[ctools][subdir] = "contrib"

; Contrib modules - additional functionality

projects[field_group][type] = "module"
projects[field_group][subdir] = "contrib"
projects[field_group][version] = "1.x-dev"

projects[field_collection][type] = "module"
projects[field_collection][subdir] = "contrib"

projects[globalredirect][type] = "module"
projects[globalredirect][subdir] = "contrib"

projects[views][type] = "module"
projects[views][subdir] = "contrib"
projects[views][version] = "3.x-dev"

projects[semanticviews][type] = "module"
projects[semanticviews][subdir] = "contrib"
projects[semanticviews][version] = "1.x-dev"

; Contrib modules - UX and layout

projects[panels][type] = "module"
projects[panels][subdir] = "contrib"
projects[panels][version] = "3.x-dev"

projects[ds][type] = "module"
projects[ds][subdir] = "contrib"
projects[ds][version] = "1.3"

projects[shadowbox][type] = "module"
projects[shadowbox][subdir] = "contrib"
projects[shadowbox][version] = "3.0-beta6"

projects[wysiwyg][type] = "module"
projects[wysiwyg][subdir] = "contrib"
projects[wysiwyg][version] = "2.1"
projects[wysiwyg][patch][356480][url] = "http://drupal.org/files/issues/lazyloadeditors-356480-68.patch"

; Libraries

libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.1/ckeditor_3.6.1.tar.gz"
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][destination] = "libraries"

; Acton core modules

projects[acton_profile][type] = "module"
projects[acton_profile][subdir] = "acton_core"
projects[acton_profile][location] = "http://drupal.anu.edu.au/fserver"

projects[anu_content_styles][type] = "module"
projects[anu_content_styles][subdir] = "acton_core"
projects[anu_content_styles][location] = "http://drupal.anu.edu.au/fserver"

projects[acton_layouts][type] = "module"
projects[acton_layouts][subdir] = "acton_core"
projects[acton_layouts][location] = "http://drupal.anu.edu.au/fserver"

; Acton theme
projects[acton][type] = "theme"
projects[acton][location] = "http://drupal.anu.edu.au/fserver"

; Acton Ready installation profile
projects[acton_ready][type] = "profile"
projects[acton_ready][location] = "http://drupal.anu.edu.au/fserver"